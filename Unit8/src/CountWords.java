import java.io.*;
import java.util.Scanner;

public class CountWords {

	public static void main(String[] args) throws IOException {
		checkNumArguments(args);
		Scanner s = null;
		
		try {
			int counter = 0;
			int total = 0;
			for(int i = 0; i < args.length; i++) {
				s = new Scanner(new BufferedReader(new FileReader(args[i])));
				while(s.hasNext()) {
					s.next();
					counter++;
				}
				System.out.println("" + args[i] + ": " + counter + " words");
				total += counter;
				counter = 0;
			}
			 System.out.println("The total of words was: " + total);
			
			
			
		} finally {
			if(s != null) {
				s.close();
			}
		}
		
	}

	private static void checkNumArguments(String[] args) {
		if(args.length < 1) {
			System.err.println("Usage: CountWords <filename>");
			System.exit(1);
		}
	}

}
