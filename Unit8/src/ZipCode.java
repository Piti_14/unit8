import java.io.*;
import java.util.Scanner;

public class ZipCode {
	private static int zipCode;
	private static String field1, field2, field3, field4, field5;
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		checkArguments(args);
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the Zip Code: ");
		zipCode = input.nextInt();
		input.close();
		
		BufferedReader file = null;
		
		
		try {
		 file = new BufferedReader(new FileReader("municipios-calles.txt"));
		 String line;
		 while((line = file.readLine()) != null) {
			 if(checkZipCode(line, zipCode)) {
				 getText(zipCode);	 
			 }
		 }
			
		} finally {
			if(file != null) {
				file.close();
			}
		}
	}
	
	private static boolean checkZipCode(String line, int code) {
		Scanner scan = new Scanner(line);
		String zip = null;
		int separators = 0;
		try {
			scan.useDelimiter(";");
			while(separators < 4) {
				zip = scan.next();
				separators++;
			}
			if(Integer.parseInt(zip) == code) {
				
				return true;
			} else {
				return false;
			}
			
		} finally {
			scan.close();
		}
	}

	private static void checkArguments(String[] args) {
		if(args.length != 1) {
			System.err.println("Usage: ZipCode <filename>");
			System.exit(1);
		}		
	}

	public static String getText(int zipCode) {
		
		
		
		
		return "hi";
	}
}
