
public class FileFormatException extends Exception{
	public FileFormatException() {
		super();
	}
	
	public FileFormatException(String msg) {
		super(msg);
	}
}

/*import java.io.*;
import java.util.*;

public class Calculate {

	public static void main(String[] args) throws IOException {
		
		checkArguments(args);
		
		BufferedReader input = null;
		
		int[] accumulators;
		String[] columnNames;
		
		try {
			
			input = new BufferedReader(new FileReader(args[0]));
			String header = input.readLine();
			int numColumns = countColumns(header);
			accumulators = new int[numColumns];
			columnNames = new String[numColumns];
			initAccumulators(accumulators);
			initColumnNames(header, columnNames);
			String line;
			while ((line = input.readLine()) != null) {
				accumulate(line, accumulators);
			}
			displayResult(columnNames, accumulators);
			
		} catch(FileNotFoundException ex) {
			System.err.println("File " + args[0] + " not found");
		} catch(IOException ex) {
			System.err.println("Error reading file " + args[0]);
		} catch(FileFormatException ex) {
			System.err.println("Wrong file format");
		} finally {
			if (input != null) {
				input.close();
			}
		}
	}
	
	private static void displayResult(String[] columnNames, int[] accumulators) {		
		for (int i = 0; i < columnNames.length; i++) {
			System.out.println(columnNames[i] + ": " + accumulators[i]);
		}		
	}

	private static void accumulate(String line, int[] accumulators) throws FileFormatException {
		Scanner scan = null;
		try {
			scan = new Scanner(line);
			scan.useDelimiter(",");
			for (int i = 0; i < accumulators.length; i++) {
				if (!scan.hasNext()) {
					throw new FileFormatException();
				}
				accumulators[i] += scan.nextInt();
			}
		} finally {
			scan.close();	
		}		
		
	}

	private static int countColumns(String header) {		
		Scanner scan = null;
		scan = new Scanner(header);
		scan.useDelimiter(",");
		int counter = 0;
		while (scan.hasNext()) {
			scan.next();
			counter ++;
		}
		scan.close();
		return counter;
	}

	private static void checkArguments(String[] args) {
		if (args.length != 1) {
			System.err.println("Usage: Calculate <filename>");
			System.exit(1);
		}
	}
	
	private static void initAccumulators(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			arr[i] = 0;
		}
	}
	
	private static void initColumnNames(String header, String[] arr) {
		Scanner scan = null;
		scan = new Scanner(header);
		scan.useDelimiter(",");
		for (int i = 0; i < arr.length; i++) {
			arr[i] = scan.next();
		}
		scan.close();	
	}

}*/

