import java.io.*;
import java.util.Scanner;

public class CSVReader {

	public static void main(String[] args) throws IOException {
		
		checkArguments(args);
		int[] accumulators;
		String[] columnNames;
		BufferedReader file = null;
		try {
			file = new BufferedReader(new FileReader(args[0]));
			String header = file.readLine();
			accumulators = new int[numberOfFields(header)];
			columnNames = new String[numberOfFields(header)];
			initAccumulators(accumulators);
			initColumnNames(header, columnNames);
			
			String line;
			while((line = file.readLine()) != null) {
				accumulate(line, accumulators);
			}
			displayResult(columnNames, accumulators);
			
		} catch(FileNotFoundException ex){
			System.err.println("File: " + args[0] + " not found.");
		} catch(IOException ex){
			System.err.println("Error reading file: " + args[0]);
		} catch(FileFormatException ex){
			System.err.println("Wrong file format");
		} finally {
			if(file != null) {
				file.close();
			}
		}
	}

	private static void displayResult(String[] arr, int[] arr2) {
		for(int i = 0; i < arr.length; i++) {
			System.out.println(arr[i] + ": " + arr2[i]);
		}
		
	}

	private static void accumulate(String line, int[] array) throws FileFormatException{
		Scanner scan = new Scanner(line);
		try {
			scan.useDelimiter(",");
			for(int i = 0; i < array.length; i++) {
				array[i] += scan.nextInt();
			}
		} finally {
			scan.close();
		}		
	}

	private static void checkArguments(String[] args) {
		if(args.length != 1) {
			System.err.println("Usage: CSVReader <filename>");
			System.exit(1);
		}
		
	}

	private static int numberOfFields(String line) {
		int fields = 0;
		Scanner scan = new Scanner(line);
		scan.useDelimiter(",");
		while(scan.hasNext()) {
			fields++;
			scan.next();
		}
		scan.close();
		return fields;
	}
	
	private static void initAccumulators(int[] array) {
		for(int i = 0; i < array.length; i++) {
			array[i] = 0;
		}
	}
	
	private static void initColumnNames(String header, String[] array) {
		Scanner scan = new Scanner(header);
		scan.useDelimiter(",");
		for(int i = 0; i < array.length; i++) {
			array[i] = scan.next();
		}
		scan.close();
	}
}
